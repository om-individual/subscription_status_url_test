const express = require('express')
const app = express()
const cors = require('cors')

app.use(cors())

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.post('/',function(req,res){
    console.log(res);
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})